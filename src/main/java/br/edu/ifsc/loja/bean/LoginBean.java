package br.edu.ifsc.loja.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import br.edu.ifsc.loja.controller.LoginController;
import br.edu.ifsc.loja.controller.UsuarioController;
import br.edu.ifsc.loja.model.Usuario;


@ManagedBean
@ViewScoped
public class LoginBean  implements Serializable {

	private LoginController controller = new LoginController();
	
	private static final long serialVersionUID = 1L;
	private Usuario usuario = new Usuario();

	public String deslogar() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("usuarioLogado");
		return "index.jsf?faces-redirect=true";
	}

	public String efetuaLogin() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		this.usuario = controller.getUsuario(usuario);
		
		if (this.usuario != null) {
			context.getExternalContext().getSessionMap().put("usuarioLogado", this.usuario);
		} else {
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage("Usuário não encontrado"));
		}		
		return "index.jsf?faces-redirect=true";
		
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public List<Usuario> listUsuario(){
		return UsuarioController.getInstance().lista();
	}
	
	public String cadastrar() {
		if ( this.usuario != null ){
			this.controller.cadastra(this.usuario);
		} 
		return "index.jsf?faces-redirect=true";
	}

}
