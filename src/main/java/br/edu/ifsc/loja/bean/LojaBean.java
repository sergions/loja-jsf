package br.edu.ifsc.loja.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import com.sun.faces.facelets.tag.jstl.core.ForEachHandler;

import br.edu.ifsc.loja.controller.CarrinhoController;
import br.edu.ifsc.loja.controller.ProdutoController;
import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.ItemCompra;
import br.edu.ifsc.loja.model.Produto;
import br.edu.ifsc.loja.model.Usuario;

@ManagedBean
@ViewScoped
public class LojaBean implements Serializable{

	private static final long serialVersionUID = 7075754873356864341L;
	
	private List<Produto> produtos;
	
	private Carrinho carrinho;

	private Usuario usuario;

	public List<Produto> getProdutos() {
		if (this.produtos == null){
			this.produtos = ProdutoController.getInstance().lista();
		}
		return produtos;
	}



	public Carrinho getCarrinho() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (this.carrinho == null) {

			this.usuario = (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado");

			this.carrinho = CarrinhoController.getInstance().getCarrinhoUsuario(this.usuario);
		}
		return carrinho;
	}

	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public void updateTotal(){
		CarrinhoController controller = CarrinhoController.getInstance();
		controller.updateTotal(carrinho);
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void add(Produto produto){
		if (this.carrinho == null){
			FacesContext context = FacesContext.getCurrentInstance();
			this.usuario = (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado");
			if (this.usuario != null){
				this.carrinho = CarrinhoController.getInstance().getCarrinhoUsuario(this.usuario);
			}
		}
		CarrinhoController.getInstance().adiciona(carrinho, produto, 1L);
	}
	
	public void remove(Long produtoId){
		CarrinhoController.getInstance().removeItem(carrinho, produtoId);
	}
	
}
