package br.edu.ifsc.loja.controller;

import java.math.BigDecimal;

import br.edu.ifsc.loja.dao.CarrinhoDAO;
import br.edu.ifsc.loja.dao.ProdutoDAO;
import br.edu.ifsc.loja.model.Carrinho;
import br.edu.ifsc.loja.model.ItemCompra;
import br.edu.ifsc.loja.model.Produto;
import br.edu.ifsc.loja.model.Usuario;

public class CarrinhoController {

	private static CarrinhoController instance;

	public static synchronized CarrinhoController getInstance(){
	
		if (instance == null){
			instance = new CarrinhoController();
		}
		return instance;
	}
	
	public void adiciona(Carrinho carrinho, Produto produto, Long quantidade){

		ItemCompra itemCompra = carrinho.getItemByProduto(produto.getId());

		if (itemCompra == null) {
		
			itemCompra = new ItemCompra(produto, quantidade);
			carrinho.addItemCompra(itemCompra);

		} else {
			carrinho.updateItemCompra(itemCompra, quantidade);

		}


	}
	
	public Carrinho getCarrinhoUsuario(Usuario usuario) {
		return CarrinhoDAO.getInstance().getCarrinhoUsuario(usuario);
	}

	public void updateTotal(Carrinho carrinho) {
		BigDecimal total = new BigDecimal(0);
		
		for (ItemCompra itemCompra : carrinho.getItens()) {
			total = total.add(itemCompra.getProduto().getValor().multiply(new BigDecimal(itemCompra.getQuantidade())));
		}
		
		carrinho.setTotal(total);
	}
	
	public void removeItem(Carrinho carrinho, Long idProduto){
		carrinho.removeItem(idProduto);
	}
}
