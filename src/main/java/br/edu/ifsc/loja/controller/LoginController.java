package br.edu.ifsc.loja.controller;

import br.edu.ifsc.loja.dao.UsuarioDAO;
import br.edu.ifsc.loja.model.Usuario;


public class LoginController  {

	public Usuario getUsuario(Usuario usuario) {
	
		Usuario usuarioLogado = UsuarioDAO.getInstance().buscaPorEmailESenha(usuario.getLogin(), usuario.getSenha());
		
		return usuarioLogado;
	}

	public void cadastra(Usuario usuario) {
		if (usuario != null){
			UsuarioDAO.getInstance().insert(usuario);
		}
	}

}
