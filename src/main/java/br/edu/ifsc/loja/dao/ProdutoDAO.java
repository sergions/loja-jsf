package br.edu.ifsc.loja.dao;

import java.math.BigDecimal;

import br.edu.ifsc.loja.model.Produto;

public class ProdutoDAO extends GenericDAO<Produto>{
	
	private static ProdutoDAO instancia;

	public static synchronized ProdutoDAO getInstance() {
		if (instancia == null) {
			instancia = new ProdutoDAO();
		}
		return instancia;
	}

	public ProdutoDAO() {
		super(Produto.class);
		this.geraMock();
	}
	
	private void geraMock(){
		Produto p = new Produto(
				10, 
				"Millennium Falcon","Uma espaçonave lendária, apesar de suas origens humildes e exterior desgastado, a Millennium Falcon desempenha um papel importante em algumas das maiores vitórias da Aliança Rebelde sobre o Império. Na superfície, a Falcon parece um calhambeque surrado. Sob seu casco, no entanto, ela guarda muitos segredos poderosos. Seus donos fizeram 'modificações especiais' no cargueiro com o passar dos anos, aumentando sua velocidade, escudo e desempenho para níveis impressionantes e absolutamente ilegais. A Falcon paga um preço alto por esses truques e melhorias. Ela pode ser imprevisível e seu hiperpropulsor recondicionado falha com frequência. Seu Capitão atual, Han Solo, até já foi visto reiniciando um motor de partida com defeito dando um soco em um anteparo.",
				new BigDecimal(513.50),
				"http://img.lum.dolimg.com/v1/images/Millennium-Falcon_018ea796.jpeg?region=0%2C1%2C1536%2C864&width=768"
				);
		this.geraIdEAdiciona(p);
		p = new Produto(
				11,"X-Wing da Resistência","A encarnação moderna de um design clássico, o caça X-wing Incom T-70 é a nave de combate característica das forças da Resistência na sua luta contra a Primeira Ordem. Quando as missões pedem por isso, o piloto ás da Resistência Poe Dameron torna-se o Líder Preto, pilotando um caça estelar X-wing T-70 de casco escuro na ação.",
				new BigDecimal(987.98),
				"http://img.lum.dolimg.com/v1/images/ep7_ia_162337_j_a7dd0845.jpeg?region=122%2C0%2C1164%2C654&width=768"
				);
		this.geraIdEAdiciona(p);
		p = new Produto(
				12,
				"Speeder","Para um transporte rápido através das dunas cobertas de lixo de Jakku, Rey depende de seu velho speeder reaproveitado",
				new BigDecimal(777.10),
				"http://img.lum.dolimg.com/v1/images/x968_teaserpubstillsdenoised3_16int_95c47825.jpeg?region=119%2C0%2C1165%2C654&width=768"
				);
		this.geraIdEAdiciona(p);
		p = new Produto(
				13,
				"Walker AT-AT","O Transporte Blindado para Todo Terreno, ou walker AT-AT, é um veículo de transporte e combate dotado de quatro pernas, usado pelas forças terrestres do Império. Com mais de 20 metros de altura e blindagem à prova de blaster, estas máquinas possantes são usadas não apenas para efeito tático, mas também para efeito psicológico",
				new BigDecimal(1302.22),
				"http://img.lum.dolimg.com/v1/images/AT-AT_89d0105f.jpeg?region=214%2C19%2C1270%2C716&width=768http://img.lum.dolimg.com/v1/images/AT-AT_89d0105f.jpeg?region=214%2C19%2C1270%2C716&width=768"
				);
		this.geraIdEAdiciona(p);
		p = new Produto(
				20,
				"Star Destroyer Imperial","O Star Destroyer Imperial é uma nave capital vibrante equipada com armas. Turbolasers e projetores de raio de tração ocupam sua superfície. Seu hangar pode lançar caças TIE, naves de embarque, unidades de assalto terrestre, sondas hiperespaciais ou ser usado para prender naves capturadas. Sua ponte movimentada é frequentada pelos melhores tripulantes da Frota Imperial. O Star Destroyer é reconhecido como a nave característica da frota Imperial. Sua presença em um sistema marca assuntos de extrema importância Imperial. No entanto, como é típico do Império, nem mesmo o Star Destroyer é suficiente para saciar a fome Imperial por demonstrações de poder. Naves maiores, como a Super Star Destroyer, parecem anões perto desses gigantes.",
				new BigDecimal(33.10),
				"http://img.lum.dolimg.com/v1/images/Star-Destroyer_ab6b94bb.jpeg?region=0%2C0%2C1600%2C900&width=768"
				);
		this.geraIdEAdiciona(p);
		p = new Produto(
				22,
				"Caça Estelar X-Wing","O X-wing é um versátil caça estelar da Aliança Rebelde que equilibra velocidade e poder de fogo. Armado com quatro canhões de laser e dois lançadores de torpedos de prótons, o X-wing pode enfrentar qualquer coisa que o Império lance contra ele. Os motores ágeis conferem uma vantagem ao X-wing durante os combates e ele pode realizar mergulhos de longo alcance com seu hiperpropulsor e seu copiloto droide astromecânico. Luke Skywalker é famoso por destruir a Estrela da Morte atrás dos controles de um X-wing.",
				new BigDecimal(999.99),
				"http://img.lum.dolimg.com/v1/images/X-Wing-Fighter_47c7c342.jpeg?region=0%2C1%2C1536%2C864&width=768"
				);
		this.geraIdEAdiciona(p);
		p = new Produto(
				30,
				"Caça Tie","O caça Tie é o símbolo inesquecível da frota imperial. Transportados a bordo de Star Destroyers e estações de batalha, os caças TIE eram conduzidos por um piloto e foram projetados para combates rápidos com os X-wings dos rebeldes e outros caças estelares. O icônico caça TIE levou a outros modelos na família TIE, incluindo o interceptor TIE, em forma de punhal, e o bombardeiro TIE carregado de explosivos. O ruído aterrorizante dos motores de um TIE amedrontam os corações de todos os inimigos do Império.",
				new BigDecimal(123.00),
				"http://img.lum.dolimg.com/v1/images/TIE-Fighter_25397c64.jpeg?region=0%2C1%2C2048%2C1152&width=768"
				);
		this.geraIdEAdiciona(p);
	}
}
