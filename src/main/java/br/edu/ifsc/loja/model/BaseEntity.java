package br.edu.ifsc.loja.model;

public abstract class BaseEntity{

	public abstract Long getId();

	public abstract void setId(Long id);
	
}
