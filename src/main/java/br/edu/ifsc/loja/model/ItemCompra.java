/*
 * @author: Sérgio Nicolau da Silva
 */
package br.edu.ifsc.loja.model;

public class ItemCompra extends BaseEntity {
	
	private Long id;
	
	private Long quantidade;
	
	private Produto produto;

	@SuppressWarnings("unused")
	private ItemCompra(){
		
	}
	
	public ItemCompra(Produto produto, Long quantidade){
		this.quantidade = quantidade;
		this.produto = produto;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	@Override
	public String toString() {
		return produto.getNome() + ": " +quantidade;
	}

}
